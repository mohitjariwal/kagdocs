-- Check last_day_stock_price value without 0.00000 and update_dt should be current_timestamp
SELECT *
FROM   [dbo].[cc_company_detail]
WHERE  CONVERT(DATE, [update_dt]) = CONVERT(DATE, Getdate())
AND last_day_stock_price_value = 0

-- Check any active ticker not geting update today
SELECT * FROM [dbo].[cc_company_detail]
WHERE [status] = 'A' 
	  AND CONVERT(DATE,[update_dt]) <> CONVERT(DATE, Getdate())
ORDER BY [ticker_code]

-- Check all 62 and 63 metric value and update_dt should be current_timestamp
SELECT *
FROM   [dbo].[cc_consensus_metric]
WHERE  [metric_id] IN ( 62, 63 )
       AND CONVERT(DATE, [update_dt]) = CONVERT(DATE, Getdate())

SELECT * FROM cc_actual_ftp_ticker WHERE [status] IN ('I','R')

-- This table contains Suppresed anlalyst tickers all status must be P means published
SELECT * FROM [dbo].[cc_publish_ticker_detail]
	   
-- This history table gives how many tickers got Initiated and Revised in actual table and make sure all the tickers should present in FTP
SELECT DISTINCT aft.[ticker_code],
			    i.[industry_name],
			    aft.[status],
			    CONVERT(DATE,aft.[create_dt]) AS create_dt,
			    CONVERT(DATE,aft.[update_dt]) AS update_dt
FROM   [dbo].[cc_actual_ftp_ticker_history] aft
LEFT OUTER JOIN [dbo].[cc_company_detail] cd ON(cd.[ticker_code] = aft.[ticker_code] AND cd.[status] = 'A')
LEFT OUTER JOIN [dbo].[cc_industry] i ON(i.[industry_id] = cd.[industry_id])
WHERE  CONVERT(DATE, aft.[create_dt]) = CONVERT(DATE,DATEADD(d,-1,Getdate()))
ORDER BY i.[industry_name],aft.[ticker_code]

-- Check any ticker metric got changed
SELECT DISTINCT [ticker_code] 
FROM [dbo].[cc_ticker_metric_mapping] 
WHERE CONVERT(DATE,(COALESCE([update_dt],[create_dt]))) = CONVERT(DATE,GETDATE())
ORDER BY [ticker_code]

SELECT [ticker_code] AS [TickerCode], 
       COALESCE(CONVERT(DATE,[update_dt]),CONVERT(DATE,[create_dt])) AS [LastUpdateDate],
       [status] AS [PublishStatus]
FROM [dbo].[cc_cancel_last_published_ticker_status] 
WHERE COALESCE(CONVERT(DATE,[update_dt]),CONVERT(DATE,[create_dt])) < '02-09-2018'
	  AND [ticker_code] IN(SELECT [ticker_code] FROM [dbo].[cc_company_detail] WHERE [status] = 'A')
ORDER BY COALESCE(CONVERT(DATE,[update_dt]),CONVERT(DATE,[create_dt])) DESC,
		 [ticker_code]

-- Update Company last_day_stock_price_value Manually
UPDATE cc_company_detail set last_day_stock_price_value = 2.53, update_id = 'SYSTEM', update_dt = CURRENT_TIMESTAMP WHERE ticker_code = 'JCP' AND status = 'A'
UPDATE cc_company_detail set last_day_stock_price_value = 36.5, update_id = 'SYSTEM', update_dt = CURRENT_TIMESTAMP WHERE ticker_code = 'AAN' AND status = 'A' AND last_day_stock_price_value = 0

-- Run this SP to update price metrics mannual 
usp_cc_update_price_metrics

-- Run this SP to update timestamp mannualy for fly metrics
usp_update_pricemetric_timestamp

-- If cc_publish_ticker_detail has status U, get those tickers and republish mannualy by using following SP
usp_cc_manage_consensus_by_ticker 'YUM','SchedulerPublish','',''

SELECT aft.ticker_code,
	   i.industry_name,
	   aft.[status],
	   aft.create_dt,
	   aft.update_dt
FROM   cc_actual_ftp_ticker aft
LEFT OUTER JOIN cc_company_detail cd ON(cd.ticker_code = aft.ticker_code AND cd.[status] = 'A')
LEFT OUTER JOIN cc_industry i ON(i.industry_id = cd.industry_id)
WHERE  CONVERT(DATE, aft.update_dt) = CONVERT(DATE, Getdate()) 
ORDER BY i.industry_name,aft.ticker_code

Schedular Name			 UTC Time	 IST Time	  End Date
--------- ----			 --- ----	 --- ----	  --- ----
FTP Write                2:00 UTC    7:30am IST   12-31-2020
Archival Runs            2:00 UTC    7:30am IST	  19-05-2019
DB Backup AM             2:30 UTC    8:00am IST	  12-31-2018
Recalculate Consensus    3:30 UTC    9:00am IST	  12-31-2018
DB Backup PM             11:30 UTC   5:00pm IST	  12-31-2018
Price Metric Update      3:30 UTC    9:00am IST	  12-31-2018

--usp_cc_get_analyst_status_by_tickercode 'KHC'

--usp_get_active_ticker_code
--usp_cc_get_analyst_status_by_tickercode_vinoth

-- Install-Package ServiceStack.Redis -Version 4.0.62
-- Install-Package StackExchange.Redis.StrongName -Version 1.1.608

--2YEAR CALCULATION
-- ADD => Q3E2017 + Q3A2016
-- DIVIDE => (Q3E2017 - Q3A2016) / Q3A2016 * 100

--Welcome to Alpha Vantage! Your API key is: PIA60K6JEGO8CE3S. Please record this API key for lifetime access to Alpha Vantage.

SELECT i.[industry_name] AS [Industry Name],
       UPPER(cd.[ticker_code]) AS [Ticker Code],
       cd.[company_name] AS [Company Name]
FROM   [dbo].[cc_company_detail] cd
       JOIN [dbo].[cc_industry] i
         ON i.[industry_id] = cd.[industry_id]
            AND cd.[status] = 'A'
            AND i.[status] = 'A'
ORDER  BY i.[display_priority],
          cd.[ticker_code]