-- Manhattan Archival Last History ID (Run on Manhattan_Archival)
select top 1 history_id from cc_actual_detail_history order by history_id desc
select top 1 history_id from cc_actual_metric_history order by history_id desc
select top 1 history_id from cc_analysis_detail_history order by history_id desc
select top 1 history_id from cc_company_detail_history order by history_id desc
select top 1 history_id from cc_company_subsidary_history order by history_id desc
select top 1 history_id from cc_consensus_history order by history_id desc
select top 1 history_id from cc_consensus_history_old order by history_id desc
select top 1 history_id from cc_consensus_metric_history order by history_id desc
select top 1 history_id from cc_consensus_recommendation_history order by history_id desc
select top 1 history_id from cc_estimated_metric_history order by history_id desc
select top 1 history_id from cc_industry_history order by history_id desc
select top 1 history_id from cc_industry_metric_category_history order by history_id desc
select top 1 history_id from cc_industry_metric_history order by history_id desc
select top 1 history_id from cc_kag_normalization_history order by history_id desc
select top 1 history_id from cc_metric_category_history order by history_id desc
select top 1 history_id from cc_metric_history order by history_id desc
select top 1 history_id from cc_sub_industry_history order by history_id desc
select top 1 history_id from cc_virtua_data_exchange_history order by history_id desc

-- Archival Count (Run on Manhattan_Archival)
select t.name ,s.row_count from sys.tables t join sys.dm_db_partition_stats s
ON t.object_id = s.object_id and t.type_desc = 'USER_TABLE' and t.name  like '%history%'
and s.index_id = 1 order by name

-- Manhattan Count (Run on Manhattan)
select t.name ,s.row_count from sys.tables t join sys.dm_db_partition_stats s
ON t.object_id = s.object_id and t.type_desc = 'USER_TABLE' and t.name  in (
'cc_actual_detail_history',
'cc_actual_metric_history',
'cc_analysis_detail_history',
'cc_company_detail_history',
'cc_company_subsidary_history',
'cc_consensus_history',
'cc_consensus_history_old',
'cc_consensus_metric_history',
'cc_consensus_recommendation_history',
'cc_estimated_metric_history',
'cc_industry_history',
'cc_industry_metric_category_history',
'cc_industry_metric_history',
'cc_kag_normalization_history',
'cc_metric_category_history',
'cc_metric_history',
'cc_sub_industry_history',
'cc_virtua_data_exchange_history') 
and s.index_id = 1 order by name



-- Manhattan Last History ID (Run on Manhattan)
select top 1 history_id from cc_consensus_history_pitr order by history_id desc
select top 1 history_id from cc_consensus_metric_history_pitr order by history_id desc

-- Archival Count (PITR)  (Run on Manhattan)
select t.name ,s.row_count from sys.tables t join sys.dm_db_partition_stats s
ON t.object_id = s.object_id and t.type_desc = 'USER_TABLE' and t.name in(
'cc_consensus_history_pitr',
'cc_consensus_metric_history_pitr')
and s.index_id = 1 order by name

-- Manhattan Count (Consensus) (Run on Manhattan)
select t.name ,s.row_count from sys.tables t join sys.dm_db_partition_stats s
ON t.object_id = s.object_id and t.type_desc = 'USER_TABLE' and t.name in(
'cc_consensus_history',
'cc_consensus_metric_history')
and s.index_id = 1 order by name